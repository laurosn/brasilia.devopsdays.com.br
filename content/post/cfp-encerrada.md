+++
date = "2016-08-26T16:09:49-03:00"
PublishDate = "2016-10-01T00:13:49-03:00"
categories = ["Announcement", "News"]
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Proposals"]
draft = true
image = "img/post-bg.jpg"
title = "Chamada de trabalhos encerrada"
description = "É! realemente encerramos a chamada de trabalhos!"

+++

Obtivemos vários ótimos retornos e nossa grade está completa.

Ainda assim contamos que você venha fazer parte do time e que participe do evento.

Veja nossa grade de palestras.

Nos encontramos no evento.
