+++
categories = ["Announcement", "News"]
date = "2016-09-17T18:22:45-03:00"
description = "Participe também do primeiro evento DevOpsDays de Brasília"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Registration"]
title = "Inscrições abertas"

+++

Venha participar do primeiro DevOpsDays de Brasília!

Teremos várias palestras e atividades para um dia inteiro de troca de experiências entre os vários presentes e palestrantes.

Fique antenado com o [programa](/about/programa) que iremos proporcionar nesse que é o primeiro **DevOpsDays** da capital federal.

Use o código de desconto ***DC61975969*** que dará **20% de desconto** para inscrições antecipadas (*Early Bird*). É limitado o número de inscrições com esse código, então não perca tempo.

Para detalhes, acesse o link [inscrições
 <sup><b>*</b><i class="fa fa-external-link"></i></sup>](http://www.devopsdaysbrasilia.eventize.com.br) e registre-se o quanto antes e aproveite o desconto para inscrições antecipadas.

Nos encontramos lá!

Abraços.

<b>*</b> <small>Site externo do parceiro ***Eventize*** para registro e gerenciamento de inscrições.</small>
