+++
categories = ["Development", "Operation"]
date = "2016-08-26T20:48:04-04:00"
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "O programa"
description = ""

+++

Teremos três formas de sessões:

1. Palestra de 25 minutos
1. Debate ou painel de 25 minutos
1. Mini-Palestra de 12 minutos

Durante o evento também teremos espaços para:

- **Ignições**: serão apresentadas durante as sessões *ignite*^[***ignite***: <https://www.devopsdays.org/ignite-talks-format/>]. Serão 4 sessões com até  7 minutos cada e com slides trocando automaticamente. Estes ignites acontecerão no auditório principal.
- **Sessões de Espaço Aberto**: As sessões iniciadas nos *ignites* serão levadas para os espaços abertos (*open spaces*^[***open spaces***: <https://www.devopsdays.org/pages/open-space-format>]) onde a discussão irá se aprofundar naqueles temas.
- **Roda Viva**: Oportunidades de debates de ampla participação do plublico presente como debatedor presente no centro da Roda (Open Fishbowl^[***Fishbowl***: <https://en.wikipedia.org/wiki/Fishbowl_(conversation)/>])


<div class = "row">
  <div class = "col-md-4">
    <h3>Temas das trilhas</h3>
  </div>
</div>
<div class = "row">
  <div class = "col-md-2">
  </div>
  <div class = "col-md-8">
    <b>CAMS</b>:
      <ul>
        <li class="text-primary"><b>[C]</b>ulture (Cultura)</li>
        <li class="text-success"><b>[A]</b>utomation (Automação)</li>
        <li class="text-info"><b>[M]</b>etrics (métricas, monitoramento, gestão)</li>
        <li class="text-warning"><b>[S]</b>haring (compartilhamento)</li>
      </ul>
  </div>
</div>

<div class = "row">
  <div class = "col-md-11">

    <h3>1<sup>o</sup> dia - 18 novembro</h3>

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-xs-12 col-sm-6 col-md-8 col-md-2 col-md-offset-0">
        <time>08:00-08:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        Credenciamento
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>08:30-09:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        Abertura
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>09:00-09:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <a href="/program/gutocarvalho" class="text text-primary">
          Introdução a Cultura DevOps<br />
          <b>Palestrante:</b> Guto Carvalho <br />
          Co-palestrante: Miguel Di Ciurcio Filho
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>09:30-10:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        Coffee-break
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>10:00-10:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box text-primary">
        <i>Slot 2 <b>[C]</b></i>:
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>10:30-11:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box text-success">
        <i>Slot 3 <b>[A]</b></i>: <br />
        <a href="/program/inajaraleppa" class="text text-success">
          Produção nove vezes ao dia - Como um time da rackspace torna isso possível?<br />
          <b>Palestrante:</b> Inajara Leppa <br />
          Co-palestrante: Bárbara Hartmann
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>11:00-11:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box text-info">
        <i>Slot 4 <b>[M]</b></i>:
        <br />
        <a href="/program/gustavo-hoyer" class="text text-info">
          Gestão centralizada de logs com ELK<br />
          <b>Palestrante: </b>Gustavo Hoyer<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>11:30-12:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box text-warning">
        <i>Slot 5 <b>[S]</b></i>:
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>12:00-13:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        Almoço
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>13:30-14:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box text-success">
        <i>Slot 6 <b>[A]</b></i>:
        <br />
        <a href="/program/rafael-gomes" class="text text-success">
          Docker Cluster - Swarm<br />
          <b>Palestrante: </b>Rafael Gomes<br />
        </a>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>14:00-14:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box text-info">
        <i>Slot 7 <b>[M]</b></i>:
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>14:30-15:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        <i>Slot 8 <b>Lightning talks</b></i>:
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>15:30-16:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        <i>Slot 9 <b>Lightning talks</b></i>:
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>16:00-16:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        Coffee-break
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>16:30-17:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        Auditório
      </div>
      <div class = "col-md-8 box">
        <i>Ignites</i>:
        <br /><br />
        <div class = "row">
          <div class = "col-md-14">
            <b>Ignite 1</b>: Puppet
          </div>
        </div>
        <div class = "row">
          <div class = "col-md-14">
            <b>Ignite 2</b>: Docker
          </div>
        </div>
        <div class = "row">
          <div class = "col-md-14">
            <b>Ignite 3</b>: GitLab
          </div>
        </div>
        <div class = "row">
          <div class = "col-md-14">
            <b>Ignite 4</b>: ELK
          </div>
        </div>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>17:00-18:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
        4 salas
      </div>
      <div class = "col-md-8 box">
        <i>Open spaces <b></b></i>:
        <br /><br />
        <div class = "row">
          <div class = "col-md-8">
            <b>OpenSpace 1</b>: Puppet
          </div>
        </div>
        <div class = "row">
          <div class = "col-md-8">
            <b>OpenSpace 2</b>: Docker
          </div>
        </div>
        <div class = "row">
          <div class = "col-md-8">
            <b>OpenSpace 3</b>: GitLab
          </div>
        </div>
        <div class = "row">
          <div class = "col-md-8">
            <b>OpenSpace 4</b>: ELK
          </div>
        </div>
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>18:30-19:30</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->

      </div>
      <div class = "col-md-8 box">
        <i>Roda viva (Open Fishbowl) <b>[CAMS]</b></i>:
      </div>
    </div> <!-- end timeslot div -->

    <!-- this div is repeated for each timeslot -->
    <div class = "row">
      <div class = "col-md-2 col-md-offset-0">
        <time>19:30-20:00</time>
      </div>
      <div class = "col-md-2">
        <!-- room -->
      </div>
      <div class = "col-md-8 box">
        Encerramento e informes
      </div>
    </div> <!-- end timeslot div -->

  </div><!-- end day 1 -->

</div>
