+++
categories = ["Development", "Operation"]
date = "2016-08-23T11:30:05-03:00"
description = ""
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "contatos"
email_organizers = "organizers-brasilia-2016@devopsdays.org"

+++

## DevOpsDays Brasília Local Team

<ul>
  <li>
    Guto Carvalho
    - <a class="btn-link" href="http://twitter.com/gutocarvalho">@gutocarvalho</a>
  </li>

  <li>
  Taciano Tres
  - <a class="btn-link" href="http://twitter.com/tacianot">@tacianot</a>
  </li>

  <li>
  Adriano Vieira
  - <a class="btn-link" href="http://twitter.com/adriano_vieira">@adriano_vieira</a>
  </li>

  <li>
  Rafael Gomes
  - <a class="btn-link" href="http://twitter.com/gomex">@gomex</a>
  </li>

  <li>
    Lauro Silveira
  </li>

  <li>
    Aline Hubner
  </li>

  <li>
    Dirceu Silva
  </li>

  <li>
    Eustáquio Guimarães
  </li>

  <li>
    Diego Aguilera
  </li>

  <li>
    Rogério Fernandes Pereira
  </li>
</ul>

Caso queira você poderá entrar em contato conosco:

- pelo email: [<i class="fa fa-envelope-o"></i>`organizers-brasilia-2016@devopsdays.org`](mailto: organizers-brasilia-2016@devopsdays.org)
- via chat (*Telegram*): [<i class="fa fa-question-circle-o"></i>`https://telegram.me/DevOpsDaysBrasilia2016`](https://telegram.me/DevOpsDaysBrasilia2016)
