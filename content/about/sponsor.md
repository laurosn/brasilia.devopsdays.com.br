+++
categories = ["Sponsor", "Patrocínio"]
date = "2016-08-29T09:34:44-03:00"
description = ""
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Patrocínio", "Apoio", "Sponsor"]
title = "Apoio e Patrocínio"

+++

# Por que Patrocinar?
Será uma ótima oportunidade para se conectar e selecionar pessoas envolvidas na cultura DevOps que, por ser uma novidade, ainda tem oferta de profissionais bem escassa.

Considerado um novo mercado em plena ascensão no Brasil, associar sua marca a esse evento é uma oportunidade de se consolidar como uma organização inovadora perante formadores de opinião e profissionais da área.

Será uma ótima oportunidade para fazer networking com pessoas interessadas no tema, podendo gerar negócios futuros para sua empresa.

Associar sua marca ao evento irá dar mostras de que sua empresa prioriza boas práticas de mercado em processos de desenvolvimento e operação de infraestrutura ágeis e que valoriza a sinergia de suas equipes de desenvolvimento de sistemas e operação de Datacenter, entregando serviços resilientes ao negócio e a seus clientes e usuários.

## Informações gerais

- **Palestras**: O patrocínio não dá direito a espaço de palestras no evento. Contudo, especialistas do patrocinador poderão enviar propostas normalmente, onde o conselho DevOpsDays-Brasília irá selecionar os melhores trabalhos enviados para formar a grade do evento.
- **Releases nos sites**:  Cabe à organização do evento definir a quantidade de releases a ser enviado aos inscritos.
- **Panfletos**: Os patrocinadores Gold e Silver poderão distribuir panfletos e brindes durante o coffee break.
- **Logotipos**: Os logotipos deverão ser entregues em formato vetorial ou em alta resolução até 2 meses antes da realização do evento. O não envio dentro do prazo poderá inviabilizar a produção do material impresso com o logo.
- **Inscrições**: A organização irá emitir códigos promocionais até 30 dias antes do evento. Os patrocinadores deverão orientar as pessoas de sua cota a se inscreverem normalmente no evento utilizando os códigos promocionais.
- **Mesas**: Os patrocinadores Silver e Gold poderão utilizar uma mesa de 2 metros por 0,7 metro, duas cadeiras e um ponto de energia que ficarão localizados próximos ao coffee break. Junto à mesa poderão expor um banner da empresa. A inscrição dos expositores deverá ser feita com o código do patrocinador, e cada patrocinador tem uma cota de entradas.

## Benefícios dos parceiros

Entendemos que uma maior quantidade de parcerias é benéfica para o evento, proporcionando uma ótima oportunidade para “mergulhos” mais profundos no mundo do Devops assim como networking entre os participantes. Por conta disso, optamos por trabalhar com cotas com valores mais acessíveis para esse evento.

Visando valorizar a participação de instituições na realização do evento, foram organizados grupos de parcerias. Cada grupo proporciona uma série de vantagens aos parceiros oficiais, obedecidos os requisitos definidos, e estabelece a contrapartida correspondente, conforme demonstrado na tabela abaixo.

<div>
  <table border=1 cellspacing=1>
    <tr>
    <th rowspan=2><i>Benefícios da parceria</i></th>
      <th colspan="5"><center>Grupos de parceria</center></th>
    </tr>
    <tr>
      <th><center>Apoio</center></th>
      <th><center>Bronze</center></th>
      <th><center>Silver</center></th>
      <th><center>Gold</center></th>
    </tr>
    <tr>
      <td>Entradas incluídas</td>
      <td><center>2</center></td>
      <td><center>4</center></td>
      <td><center>6</center></td>
      <td><center>8</center></td>
    </tr>
    <tr>
      <td>Release no site e comunidades</td>
      <td><center>-</center></td>
      <td><center>-</center></td>
      <td bgcolor="silver"><center>Sim</center></td>
      <td bgcolor="gold"><center>Sim</center></td>
    </tr>
    <tr>
      <td>Distribuir panfletos</td>
      <td><center>-</center></td>
      <td><center>-</center></td>
      <td bgcolor="silver"><center>-</center></td>
      <td bgcolor="gold"><center>Sim</center></td>
    </tr>
    <tr>
      <td>Sua logomarca no site do evento</td>
      <td><center>Sim</center></td>
      <td bgcolor="bronze"><center>Sim</center></td>
      <td bgcolor="silver"><center>Sim</center></td>
      <td bgcolor="gold"><center>Sim</center></td>
    </tr>
    <tr>
      <td>Sua logomarca nos crachás do evento</td>
      <td><center>Sim</center></td>
      <td bgcolor="bronze"><center>Sim</center></td>
      <td bgcolor="silver"><center>Sim</center></td>
      <td bgcolor="gold"><center>Sim</center></td>
    </tr>
    <tr>
      <td>Sua logomarca nos posters do evento</td>
      <td><center>Sim</center></td>
      <td bgcolor="bronze"><center>Sim</center></td>
      <td bgcolor="silver"><center>Sim</center></td>
      <td bgcolor="gold"><center>Sim</center></td>
    </tr>
    <tr>
      <td>Mesa + Banner próximo ao Coffee Break</td>
      <td><center>-</center></td>
      <td><center>-</center></td>
      <td bgcolor="silver"><center>Sim</center></td>
      <td bgcolor="gold"><center>Sim</center></td>
    </tr>
    <tr>
      <td>Agradecimento entre cada sessão</td>
      <td><center>-</center></td>
      <td bgcolor="bronze"><center>Sim</center></td>
      <td bgcolor="silver"><center>Sim</center></td>
      <td bgcolor="gold"><center>Sim</center></td>
    </tr>
    <tr>
      <td>Agradecimento na abertura do evento</td>
      <td><center>Sim</center></td>
      <td bgcolor="bronze"><center>Sim</center></td>
      <td bgcolor="silver"><center>Sim</center></td>
      <td bgcolor="gold"><center>Sim</center></td>
    </tr>
    <tr>
      <td>Agradecimento no encerramento</td>
      <td><center>Sim</center></td>
      <td bgcolor="bronze"><center>Sim</center></td>
      <td bgcolor="silver"><center>Sim</center></td>
      <td bgcolor="gold"><center>Sim</center></td>
    </tr>
    <tr>
      <th>Valor a investir</th>
      <th><center>R$1.000</center></th>
      <th><center>R$4.000</center></th>
      <th><center>R$6.000</center></th>
      <th><center>R$9.000</center></th>
    </tr>
  </table>

</div>

<br />
