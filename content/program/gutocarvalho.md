+++
categories = ["Talk", "Palestra"]
copalestrante_name = "Miguel Di Ciurcio Filho"
copalestrante_bio = "Miguel Di Ciurcio Filho é co-fundador da comunidade Puppet Brasil e foi o primeiro profissional certificado Puppet da América Latina. Participou do Google Summer Of Code no projeto QEMU e contribuiu com diversos projetos open source via patches, palestras e eventos. Em 2011 fundou a Instruct Infraestrutura Ágil e desde então dedica-se a oferecer consultoria e treinamentos sobre infraestrutura e automação em seu país. A Instruct é a única representante da Puppet e GitLab no Brasil oferecendo treinamentos, projetos e produtos."
copalestrante_photoUrl = "https://pbs.twimg.com/profile_images/3273701501/7ba6b3222a85027e54417170cf25fc08.jpeg"
copalestrante_twitter = "mciurcio"
copalestrante_website = "http://instruct.com.br/blog"
date = "2016-09-21T19:11:06-03:00"
description = "Guto Carvalho"
draft = false
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "gutocarvalho"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">Introdução a Cultura DevOps</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Essa palestra vai iniciar o ouvinte na cultura DevOps, o objetivo é fazer com que os iniciantes entendam como surgiu o termo, quem foram os idealizadores da ideia, quais as motivações para criação de tal movimento, como ele se organiza e também mostrar como funciona a comunidade em volta da iniciativa.

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-primary">Culture (Cultura)</span>

<!-- {{.talk.level}} Iniciante / Intermediária / Avançada -->
- **Público alvo**: Iniciante
